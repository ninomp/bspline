package ControlPoint;

use strict;

use QtCore4;
use QtCore4::isa qw(Qt::GraphicsItem);

sub NEW
{
	shift->SUPER::NEW(@_);

	this->setFlag(Qt::GraphicsItem::ItemIsMovable() | Qt::GraphicsItem::ItemIsSelectable() | Qt::GraphicsItem::ItemSendsGeometryChanges());

	this->{data_model} = undef;
}

sub setParentDataModel
{
	my ($data_model) = @_;
	this->{data_model} = $data_model;
}

sub boundingRect
{
	return Qt::RectF(-5, -5, 10, 10);
}

sub paint
{
	my ($painter, $option) = @_;

	if (${$option->state & Qt::Style::State_Selected()})
	{
		$painter->setBrush(Qt::Brush(Qt::red()));
	}
	else
	{
		$painter->setBrush(Qt::Brush(Qt::blue()));
	}

	$painter->drawEllipse(-5, -5, 10, 10);
}

sub itemChange
{
	my ($change, $value) = @_;

	if (this->{data_model} != undef)
	{
		this->{data_model}->onPointChanged(this, $change, $value);
	}

	return this->SUPER::itemChange($change, $value);
}

1;
