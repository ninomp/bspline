#!/usr/bin/env perl

use strict;

use QtCore4;
use QtGui4;

use MainWindow;

sub main
{
	my $app = Qt::Application(\@ARGV);

	my $main_window = MainWindow();
	$main_window->show();

	exit $app->exec;
}

main();
