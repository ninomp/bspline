B-spline
========

B-spline simulation in Perl with Qt4.

Uses [perlqt4](https://code.google.com/p/perlqt4/) library.


References
----------

 * [Animated B-Spline in Qt](https://github.com/vkorchagin/animated-b-spline)

