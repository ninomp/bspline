package GraphicsScene;

use strict;

use QtCore4;
use QtCore4::isa qw(Qt::GraphicsScene);

use QtCore4::signals
    mouseDoubleClicked => ['QGraphicsSceneMouseEvent *'];

sub controlPoints
{
	my @all_scene_items = @{this->items(Qt::AscendingOrder())};
	return map { ($_->isa('ControlPoint')) ? ($_->scenePos()) : () } @all_scene_items;
}

sub selectedPoints
{
	my @all_selected_items = @{this->selectedItems()};
	return map { ($_->isa('ControlPoint')) ? ($_) : () } @all_selected_items;
}

sub NEW
{
	shift->SUPER::NEW(@_);
}

sub clear
{
	my @items = @{this->items()};

	foreach my $item (@items)
	{
		if ($item->isa('ControlPoint'))
		{
			removeItem($item);
		}
	}
}

sub mouseDoubleClickEvent
{
	my ($event) = @_;
	emit mouseDoubleClicked($event);
	this->SUPER::mouseDoubleClickEvent($event);
}

1;
