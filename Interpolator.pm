package Interpolator;

use strict;

use QtCore4;

use QtCore4::isa qw(Qt::Object);

use QtCore4::signals
    orderChanged => ['int'],
    numberOfPointsChanged => ['int'];

use QtCore4::slots
    setOrder => ['int'],
    setNumberOfPoints => ['int'];

sub order
{
	return this->{order};
}

sub numberOfPoints
{
	return this->{nr_curve_points};
}

sub NEW
{
	shift->SUPER::NEW(@_);

	this->{order} = 3;
	this->{nr_curve_points} = 30;
}

sub setOrder
{
	my ($order) = @_;

	if ($order ne this->{order})
	{
		this->{order} = $order;

		emit orderChanged($order);
	}
}

sub setNumberOfPoints
{
	my ($number) = @_;

	if ($number ne this->{nr_curve_points})
	{
		this->{nr_curve_points} = $number;

		emit numberOfPointsChanged($number);
	}
}

sub interpolate
{
	my (@control_points) = @_;

	my $order = this->{order};
	my $nr_curve_points = this->{nr_curve_points};
	my $nr_control_points = scalar(@control_points);
	my $nr_knots = $nr_control_points + $order;

	my @knots = createKnots($nr_control_points, $order);

	my $parameter_max = @knots[$nr_knots];  # Equal to $nr_control_points - $order + 1
	my $parameter_step = $parameter_max / ($nr_curve_points - 1);
	my $parameter_value = 0;

	my @curve_points = ();
	for (my $i = 1; $i <= $nr_curve_points; $i++)
	{
		# Make sure that last point of curve is last control point
		if ($i == $nr_curve_points)
		{
			$parameter_value = $parameter_max;
		}

		my @basis_function = createBasisFunction($order, $parameter_value, $nr_control_points, \@knots);

		my $x = 0;
		my $y = 0;
		for (my $j = 0; $j < $nr_control_points; $j++)
		{
			$x += $basis_function[$j] * $control_points[$j]->x();
			$y += $basis_function[$j] * $control_points[$j]->y();
		}

		push(@curve_points, Qt::PointF($x, $y));

		$parameter_value += $parameter_step;
	}

	return @curve_points;
}

sub createKnots
{
	my ($nr_control_points, $order) = @_;
	my $nr_knots = $nr_control_points + $order;

	my @knots = ();
	push(@knots, 0);
	push(@knots, 0);

	for (my $i = 2; $i <= $nr_knots; $i++)
	{
		if (($i > $order) && ($i < ($nr_control_points + 2)))
		{
			push(@knots, (@knots[$i - 1] + 1));
		}
		else
		{
			push(@knots, @knots[$i - 1]);
		}
	}

	return @knots;
}

sub createBasisFunction
{
	my $order = $_[0];
	my $parameter = $_[1];
	my $nr_control_points = $_[2];
	my @knots = @{$_[3]};
	my $nr_knots = $nr_control_points + $order;

	my @temp = ();
	push(@temp, 0);

	for (my $i = 1; $i < $nr_knots; $i++)
	{
		if (($parameter >= $knots[$i]) && ($parameter < $knots[$i + 1]))
		{
			push(@temp, 1);
		}
		else
		{
			push(@temp, 0);
		}
	}

	my $first_term;
	my $second_term;

	for (my $j = 2; $j <= $order; $j++)
	{
		for (my $k = 1; $k <= ($nr_knots - $j); $k++)
		{
			if ($temp[$k] != 0)
			{
				$first_term = (($parameter - $knots[$k]) * $temp[$k]) / ($knots[$k + $j - 1] - $knots[$k]);
			}
			else
			{
				$first_term = 0;
			}

			if ($temp[$k + 1] != 0)
			{
				$second_term = (($knots[$k + $j] - $parameter) * $temp[$k + 1]) / ($knots[$k + $j] - $knots[$k + 1]);
			}
			else
			{
				$second_term = 0;
			}

			$temp[$k] = $first_term + $second_term;
		}
	}

	if ($parameter == $knots[$nr_knots])
	{
		$temp[$nr_control_points] = 1;
	}

	my @basis_function = ();

	for (my $l = 1; $l <= $nr_control_points; $l++)
	{
		push(@basis_function, $temp[$l]);
	}

	return @basis_function;
}

1;
