package MainWindow;

use strict;

use QtCore4;
use QtCore4::isa qw(Qt::MainWindow);
use QtCore4::slots
    documentNew => [],
    documentOpen => [],
    documentSave => [],
    documentSaveAs => [],
    about => [],
    export => [],
    exportAsImage => [],
    documentFilenameChanged => ['QString'];

use constant
{
    APP_NAME => "B-spline simulation",
    BSPLINE_FILE_FILTER => "B-spline (*.bsp)(*.bsp);;All Files (*.*)(*.*)"
};

use Document;
use GraphicsScene;
use ControlPointsDataModel;

sub document
{
	return this->{document};
}

sub NEW
{
	shift->SUPER::NEW(@_);

	my $document = Document();
	this->connect($document, SIGNAL 'filenameChanged(QString)', this, SLOT 'documentFilenameChanged(QString)');
	this->connect($document, SIGNAL 'modifiedChanged(bool)', this, SLOT 'setWindowModified(bool)');
	this->{document} = $document;

	my $interpolator = $document->interpolator();
	this->{interpolator} = $interpolator;

	my $graphics_view = Qt::GraphicsView();
	$graphics_view->setScene($document->graphicsScene());
	$graphics_view->setDragMode(Qt::GraphicsView::RubberBandDrag());
	this->{graphics_view} = $graphics_view;
	this->setCentralWidget($graphics_view);

	my $points_dock = Qt::DockWidget("Points");
	$points_dock->setWidget($document->tableView());
	this->addDockWidget(Qt::RightDockWidgetArea(), $points_dock);
	this->{points_dock} = $points_dock;

	my $curve_order_spinbox = Qt::SpinBox();
	$curve_order_spinbox->setRange(1, 10);
	$curve_order_spinbox->setValue(3);
	this->connect($curve_order_spinbox, SIGNAL 'valueChanged(int)', $interpolator, SLOT 'setOrder(int)');
	this->connect($interpolator, SIGNAL 'orderChanged(int)', $curve_order_spinbox, SLOT 'setValue(int)');
	this->{curve_order_spinbox} = $curve_order_spinbox;

	my $curve_points_spinbox = Qt::SpinBox();
	$curve_points_spinbox->setRange(10, 1000);
	$curve_points_spinbox->setSingleStep(10);
	$curve_points_spinbox->setValue(30);
	this->connect($curve_points_spinbox, SIGNAL 'valueChanged(int)', $interpolator, SLOT 'setNumberOfPoints(int)');
	this->connect($interpolator, SIGNAL 'numberOfPointsChanged(int)', $curve_points_spinbox, SLOT 'setValue(int)');
	this->{curve_points_spinbox} = $curve_points_spinbox;

	my $parameters_form = Qt::FormLayout();
	$parameters_form->addRow("Curve order", $curve_order_spinbox);
	$parameters_form->addRow("Curve points", $curve_points_spinbox);

	my $parameters_widget = Qt::Widget();
	$parameters_widget->setLayout($parameters_form);

	my $parameters_dock = Qt::DockWidget("Parameters");
	$parameters_dock->setWidget($parameters_widget);
	this->addDockWidget(Qt::RightDockWidgetArea(), $parameters_dock);
	this->{parameters_dock} = $parameters_dock;

	createActions();
	createMenus();
	createStatusBar();

	documentFilenameChanged();
}

sub createActions
{
	my $action_new = Qt::Action("&New", this);
	$action_new->setShortcut(Qt::KeySequence("Ctrl+N"));
	$action_new->setStatusTip("Create a new document");
	this->connect($action_new, SIGNAL 'triggered()', this, SLOT 'documentNew()');
	this->{action_new} = $action_new;

	my $action_open = Qt::Action("&Open...", this);
	$action_open->setShortcut(Qt::KeySequence("Ctrl+O"));
	$action_open->setStatusTip("Open an existing document");
	this->connect($action_open, SIGNAL 'triggered()', this, SLOT 'documentOpen()');
	this->{action_open} = $action_open;

	my $action_save = Qt::Action("&Save", this);
	$action_save->setShortcut(Qt::KeySequence("Ctrl+S"));
	$action_save->setStatusTip("Save the current document");
	this->connect($action_save, SIGNAL 'triggered()', this, SLOT 'documentSave()');
	this->{action_save} = $action_save;

	my $action_saveas = Qt::Action("Save &As...", this);
	$action_saveas->setShortcut(Qt::KeySequence("Ctrl+Shift+S"));
	$action_saveas->setStatusTip("Save the current document under a new name");
	this->connect($action_saveas, SIGNAL 'triggered()', this, SLOT 'documentSaveAs()');
	this->{action_saveas} = $action_saveas;

	my $action_export = Qt::Action("&Export...", this);
	$action_export->setStatusTip("Export curve points to file");
	this->connect($action_export, SIGNAL 'triggered()', this, SLOT 'export()');
	this->{action_export} = $action_export;

	my $action_exportimage = Qt::Action("Export as &Image...", this);
	$action_exportimage->setStatusTip("Export viewport as image");
	this->connect($action_exportimage, SIGNAL 'triggered()', this, SLOT 'exportAsImage()');
	this->{action_exportimage} = $action_exportimage;

	my $action_exit = Qt::Action("E&xit", this);
	$action_exit->setShortcut(Qt::KeySequence("Ctrl+Q"));
	$action_exit->setStatusTip("Exit the application");
	this->connect($action_exit, SIGNAL 'triggered()', this, SLOT 'close()');
	this->{action_exit} = $action_exit;

	my $action_selection_delete = Qt::Action("&Delete", this);
	$action_selection_delete->setShortcut(Qt::KeySequence("Delete"));
	$action_selection_delete->setStatusTip("Delete selected points");
	this->connect($action_selection_delete, SIGNAL 'triggered()', this->{document}, SLOT 'deleteSelection()');
	this->{action_selection_delete} = $action_selection_delete;

	my $action_select_all = Qt::Action("Select &All", this);
	$action_select_all->setShortcut(Qt::KeySequence("Ctrl+A"));
	$action_select_all->setStatusTip("Select all points");
	this->connect($action_select_all, SIGNAL 'triggered()', this->{document}, SLOT 'selectAll()');
	this->{action_select_all} = $action_select_all;

	my $action_select_none = Qt::Action("Select &None", this);
	$action_select_none->setStatusTip("Deselect all points");
	this->connect($action_select_none, SIGNAL 'triggered()', this->{document}, SLOT 'selectNone()');
	this->{action_select_none} = $action_select_none;

	my $action_selection_invert = Qt::Action("&Invert", this);
	this->connect($action_selection_invert, SIGNAL 'triggered()', this->{document}, SLOT 'selectInvert()');
	this->{action_selection_invert} = $action_selection_invert;

	my $action_points_showhide = this->{points_dock}->toggleViewAction();
	$action_points_showhide->setStatusTip("Show/hide points window");
	this->{action_points_showhide} = $action_points_showhide;

	my $action_parameters_showhide = this->{parameters_dock}->toggleViewAction();
	$action_parameters_showhide->setStatusTip("Show/hide parameters window");
	this->{action_parameters_showhide} = $action_parameters_showhide;

	my $action_about = Qt::Action("&About", this);
	$action_about->setStatusTip("Show the application's About box");
	this->connect($action_about, SIGNAL 'triggered()', this, SLOT 'about()');
	this->{action_about} = $action_about;

	my $action_aboutQt = Qt::Action("About &Qt", this);
	$action_aboutQt->setStatusTip("Show the Qt4 library's About box");
	this->connect($action_aboutQt, SIGNAL 'triggered()', Qt::qApp(), SLOT 'aboutQt()');
	this->{action_aboutQt} = $action_aboutQt;
}

sub createMenus
{
	my $file_menu = this->menuBar()->addMenu("&File");
	$file_menu->addAction(this->{action_new});
	$file_menu->addAction(this->{action_open});
	$file_menu->addAction(this->{action_save});
	$file_menu->addAction(this->{action_saveas});
	$file_menu->addSeparator();
	$file_menu->addAction(this->{action_export});
	$file_menu->addAction(this->{action_exportimage});
	$file_menu->addSeparator();
	$file_menu->addAction(this->{action_exit});

	my $selection_menu = this->menuBar()->addMenu("&Selection");
	$selection_menu->addAction(this->{action_selection_delete});
	$selection_menu->addAction(this->{action_selection_invert});
	$selection_menu->addSeparator();
	$selection_menu->addAction(this->{action_select_all});
	$selection_menu->addAction(this->{action_select_none});

	my $view_menu = this->menuBar()->addMenu("&View");
	$view_menu->addAction(this->{action_points_showhide});
	$view_menu->addAction(this->{action_parameters_showhide});

	my $help_menu = this->menuBar()->addMenu("&Help");
	$help_menu->addAction(this->{action_about});
	$help_menu->addAction(this->{action_aboutQt});
}

sub createStatusBar
{
	this->statusBar()->show();
}

sub documentNew
{
	if (this->maybeSave())
	{
		this->document()->clear();
	}
}

sub documentOpen
{
	if (this->maybeSave())
	{
		my $filename = Qt::FileDialog::getOpenFileName(this, "Open document", Qt::Dir::homePath(), BSPLINE_FILE_FILTER);
		if ($filename)
		{
			if (this->document()->load($filename))
			{
				this->statusBar()->showMessage("Loaded '$filename'", 2000);
			}
			else
			{
				Qt::MessageBox::warning(this, APP_NAME, "Cannot open '$filename'!");
			}
		}
	}
}

sub documentSave
{
	if (this->document()->isNew())
	{
		this->documentSaveAs();
	}
	else
	{
		if (this->document()->store())
		{
			this->statusBar()->showMessage("Document saved.");
		}
	}
}

sub documentSaveAs
{
	my $filename = Qt::FileDialog::getSaveFileName(this, "Save document", Qt::Dir::homePath(), BSPLINE_FILE_FILTER);
	if ($filename)
	{
		if (this->document()->store($filename))
		{
			this->statusBar()->showMessage("Saved to '$filename'", 2000);
		}
		else
		{
			Qt::MessageBox::warning(this, APP_NAME, "Cannot save to '$filename'!");
		}
	}
}

sub export
{
	my $filetypes = "Comma-delimited files (*.csv)(*.csv);;Plain text files (*.txt)(*.txt);;All files (*.*)(*.*)";
	my $filename = Qt::FileDialog::getSaveFileName(this, "Export", Qt::Dir::homePath(), $filetypes);
	if ($filename)
	{
		if (this->document()->exportCurvePoints($filename))
		{
			this->statusBar()->showMessage("Curve points exported to '$filename'", 2000);
		}
	}
}

sub exportAsImage
{
	my $filetypes = "Portable Network Graphics (*.png)(*.png)";
	my $filename = Qt::FileDialog::getSaveFileName(this, "Export as image", Qt::Dir::homePath(), $filetypes);
	if ($filename)
	{
		my $pixmap = Qt::Pixmap::grabWidget(this->{graphics_view});
		if ($pixmap->save($filename))
		{
			this->statusBar()->showMessage("Exported to '$filename'", 2000);
		}
	}
}

sub about
{
	my $title = "About Application";
	my $text = "<p>B-spline simulation with Qt4 and Perl</p><p>Copyright &copy; 2015";

	Qt::MessageBox::about(this, $title, $text);
}

sub closeEvent
{
	my ($event) = @_;

	if (this->maybeSave())
	{
		$event->accept();
	}
	else
	{
		$event->ignore();
	}
}

sub maybeSave
{
	if (this->document()->isModified())
	{
		my $text = "The document has been modified.\nDo you want to save your changes?";
		my $buttons = Qt::MessageBox::Save() | Qt::MessageBox::Discard() | Qt::MessageBox::Cancel();
		my $ret = Qt::MessageBox::warning(this, APP_NAME, $text, $buttons);

		if ($ret == Qt::MessageBox::Save())
		{
			return this->documentSave();
		}
		elsif ($ret == Qt::MessageBox::Cancel())
		{
			return 0;
		}
	}

	return 1;
}

sub documentFilenameChanged
{
	this->setWindowTitle(sprintf("%s\[*] - %s", this->document()->strippedName(), APP_NAME));
}

1;
