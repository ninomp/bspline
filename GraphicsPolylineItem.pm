package GraphicsPolylineItem;

use strict;

use QtCore4;
use QtCore4::isa qw(Qt::AbstractGraphicsShapeItem);

sub NEW
{
	shift->SUPER::NEW(@_);

	this->{polygon} = Qt::PolygonF();
}

sub setPolygon
{
	my ($polygon) = @_;

	if ($polygon == this->{polygon})
	{
		return;
	}

	this->prepareGeometryChange();

	this->{polygon} = $polygon;

	this->update();
}

sub boundingRect
{
	return this->{polygon}->boundingRect();
}

sub paint
{
	my ($painter) = @_;

	$painter->drawPolyline(this->{polygon});
}

1;
