package ControlPointsDataModel;

use strict;

use QtCore4;
use QtCore4::isa qw(Qt::AbstractTableModel);
use QtCore4::signals
    changed => [];

sub controlPoints
{
	return this->{control_points};
}

sub NEW
{
	shift->SUPER::NEW(@_);

	this->{control_points} = [];
}

sub clear
{
	beginRemoveRows(Qt::ModelIndex(), 0, rowCount() - 1);

	this->{control_points} = [];

	endRemoveRows();

	emit changed();
}

sub addPoint
{
	my ($point) = @_;

	$point->setParentDataModel(this);

	beginInsertRows(Qt::ModelIndex(), rowCount(), rowCount());

	push @{this->controlPoints}, $point;

	endInsertRows();

	emit changed();
}

sub removePoint
{
	my ($point) = @_;

	my $row = findPoint($point);

	beginRemoveRows(Qt::ModelIndex(), $row, $row);

	splice @{this->controlPoints}, $row, 1;

	endRemoveRows();

	emit changed();
}

sub findPoint
{
	my ($point) = @_;

	my @points = @{this->controlPoints};

	for (my $i = 0; $i <= $#points; $i++)
	{
		if ($points[$i] eq $point)
		{
			return $i;
		}
	}

	return -1;
}

sub pointIndex
{
	my ($point) = @_;
	return this->index(this->findPoint($point), 0, Qt::ModelIndex());
}

sub points
{
	return map { $_->scenePos() } @{this->controlPoints};
}

sub rowCount
{
	return scalar @{this->controlPoints};
}

sub columnCount
{
	return 2;
}

sub data
{
	my ($index, $role) = @_;

	if (!$index->isValid())
	{
		return Qt::Variant();
	}

	if ($index->row() < 0 || $index->row() >= rowCount())
	{
		return Qt::Variant();
	}

	if ($role == Qt::DisplayRole() | $role == Qt::EditRole())
	{
		my $point = this->controlPoints->[$index->row()];

		if ($index->column() == 0)
		{
			return Qt::Variant(Qt::String($point->x()));
		}
		elsif ($index->column() == 1)
		{
			return Qt::Variant(Qt::String($point->y()));
		}
		else
		{
			return Qt::Variant();
		}
	}

	return Qt::Variant();
}

sub headerData
{
	my ($section, $orientation, $role) = @_;

	if ($role == Qt::DisplayRole())
	{
		if ($orientation == Qt::Horizontal())
		{
			if ($section == 0)
			{
				return Qt::Variant(Qt::String("X"));
			}
			elsif ($section == 1)
			{
				return Qt::Variant(Qt::String("Y"));
			}
			else
			{
				return Qt::Variant();
			}
		}
		elsif ($orientation == Qt::Vertical())
		{
			return Qt::Variant(Qt::String($section + 1));
		}
	}

	return Qt::Variant();
}

sub setData
{
	my ($index, $value, $role) = @_;

	if ($index->isValid() && $role == Qt::EditRole())
	{
		my $points = this->{control_points};
		my $col = $index->column();
		my $row = $index->row();
		my $val = $value->value();

		if ($col == 0)
		{
			$points->[$row]->setX($val);
		}
		elsif ($col == 1)
		{
			$points->[$row]->setY($val);
		}
		else
		{
			return 0;
		}

		emit dataChanged($index, $index);

		return 1;
	}

	return 0;
}

sub flags
{
	my ($index) = @_;

	my $flags = this->SUPER::flags($index);

	if ($index->isValid())
	{
		$flags |= Qt::ItemIsEditable();
		$flags |= Qt::ItemIsDragEnabled();
	}

	return $flags | Qt::ItemIsDropEnabled();
}

sub supportedDropActions
{
	return Qt::MoveAction();
}

sub mimeTypes
{
	return [ 'application/x-bspline-controlpoints' ];
}

sub mimeData
{
	my ($indexes) = @_;

	# Extract unique row indices
	my @indices = map { ($_->isValid() && $_->column() == 0) ? ($_->row()) : () } (@{$indexes});

	# Encode extracted indices as semicolon-delimited string
	my $encodedData = Qt::ByteArray(join(";", @indices));

	my $mimeData = Qt::MimeData();

	$mimeData->setData('application/x-bspline-controlpoints', $encodedData);

	return $mimeData;
}

sub dropMimeData
{
	my ($data, $action, $row, $column, $parent) = @_;

	if (!$data->hasFormat('application/x-bspline-controlpoints'))
	{
		return 0;
	}

	if ($action == Qt::IgnoreAction())
	{
		return 1;
	}

	# Get destination index
	my $destination_index;
	if ($row >= 0)
	{
		$destination_index = $row;
	}
	elsif ($parent->isValid())
	{
		$destination_index = $parent->row();
	}
	else
	{
		$destination_index = rowCount();
	}

	# Retrieve and decode indices from received data
	my $encoded_data = $data->data('application/x-bspline-controlpoints')->data();
	my @indices = sort { $a <=> $b } (split(";", $encoded_data));

	# Temporary array for storing items that are being reordered
	my @moving_items = ();

	# Notify that a change in the layout of this model will be made
	emit layoutAboutToBeChanged();

	# Remove items in reverse order to avoid index recalculation
	foreach my $source_index (reverse(@indices))
	{
		# Decrease destination index by one for every item before insertion point
		if ($source_index < $destination_index)
		{
			$destination_index--;
		}

		# Move element to temporary array
		unshift(@moving_items, splice(this->{control_points}, $source_index, 1));
	}

	# Insert elements on the new position
	splice(this->{control_points}, $destination_index, 0, @moving_items);

	# Notify about each movement made
	foreach my $source_index (@indices)
	{
		changePersistentIndex(this->index($source_index, 0), this->index($destination_index, 0));  # First column
		changePersistentIndex(this->index($source_index, 1), this->index($destination_index, 1));  # Second column

		$destination_index++;
	}

	# Notify that change in the layout of this model has been made
	emit layoutChanged();

	# Notify about change of data
	emit changed();

	# Indicate success
	return 1;
}

sub onPointChanged
{
	my ($point, $change, $value) = @_;

	if ($change == Qt::GraphicsItem::ItemPositionHasChanged())
	{
		sendRowChangedNotification(findPoint($point));
		emit changed();
	}
}

sub sendRowChangedNotification
{
	my ($row) = @_;
	my $first_cell = this->index($row, 0);
	my $last_cell = this->index($row, 1);
	emit dataChanged($first_cell, $last_cell);
}

sub sendAllDataChangedNotification
{
	my $top_left = this->index(0, 0, Qt::ModelIndex());
	my $bottom_right = this->index(rowCount() - 1, columnCount() - 1, Qt::ModelIndex());
	emit dataChanged($top_left, $bottom_right);
}

1;
