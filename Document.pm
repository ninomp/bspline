package Document;

use strict;

use QtCore4;
use QtCore4::isa qw(Qt::Object);

use QtCore4::signals
    filenameChanged => ['QString'],
    modifiedChanged => ['bool'];

use QtCore4::slots
    controlPointsChanged => [],
    selectAll => [],
    selectNone => [],
    selectInvert => [],
    deleteSelection => [],
    sceneSelectionChanged => [],
    tableSelectionChanged => ['const QItemSelection &', 'const QItemSelection &'],
    sceneMouseDoubleClicked => ['QGraphicsSceneMouseEvent *'];

use constant
{
    FILE_SIGNATURE => "B-SPLINE"
};

use ControlPoint;
use ControlPointsDataModel;
use GraphicsScene;
use GraphicsPolylineItem;
use Interpolator;

sub filename
{
	return this->{filename};
}

sub strippedName
{
	if (this->isNew())
	{
		return "untitled";
	}
	else
	{
		return Qt::FileInfo(this->{filename})->fileName();
	}
}

sub isNew
{
	return (this->{filename} eq "") ? 1 : 0;
}

sub isModified
{
	return this->{modified};
}

sub controlPoints
{
	return this->{control_points};
}

sub tableView
{
	return this->{table_view};
}

sub graphicsScene
{
	return this->{graphics_scene};
}

sub interpolator
{
	return this->{interpolator};
}

sub NEW
{
	shift->SUPER::NEW(@_);

	this->{filename} = "";
	this->{modified} = 0;

	my $control_points = ControlPointsDataModel();
	this->connect($control_points, SIGNAL 'changed()', this, SLOT 'controlPointsChanged()');
	this->{control_points} = $control_points;

	my $table_view = Qt::TableView();
	$table_view->setModel($control_points);
	$table_view->setSelectionBehavior(Qt::AbstractItemView::SelectRows());
	$table_view->setSelectionMode(Qt::AbstractItemView::ExtendedSelection());
	$table_view->setDragEnabled(1);
	$table_view->setAcceptDrops(1);
	$table_view->setDropIndicatorShown(1);
	$table_view->setDragDropMode(Qt::AbstractItemView::InternalMove());
	this->{table_view} = $table_view;

	my $selection_model = $table_view->selectionModel();
	this->connect($selection_model, SIGNAL 'selectionChanged(const QItemSelection &, const QItemSelection &)', this, SLOT 'tableSelectionChanged(const QItemSelection &, const QItemSelection &)');
	this->{selection_model} = $selection_model;

	my $graphics_scene = GraphicsScene();
	$graphics_scene->setSceneRect(-250, -250, 500, 500);
	this->{graphics_scene} = $graphics_scene;
	this->connect($graphics_scene, SIGNAL 'selectionChanged()', this, SLOT 'sceneSelectionChanged()');
	this->connect($graphics_scene, SIGNAL 'mouseDoubleClicked(QGraphicsSceneMouseEvent *)', this, SLOT 'sceneMouseDoubleClicked(QGraphicsSceneMouseEvent *)');

	my $polyline_item = GraphicsPolylineItem();
	this->{polyline_item} = $polyline_item;
	$graphics_scene->addItem($polyline_item);

	my $interpolator = Interpolator();
	this->connect($interpolator, SIGNAL 'orderChanged(int)', this, SLOT 'controlPointsChanged()');
	this->connect($interpolator, SIGNAL 'numberOfPointsChanged(int)', this, SLOT 'controlPointsChanged()');
	this->{interpolator} = $interpolator;

	this->{bspline_points} = [];

	my $bspline_item = GraphicsPolylineItem();
	this->{bspline_item} = $bspline_item;
	$graphics_scene->addItem($bspline_item);
}

sub controlPointsChanged
{
	my @control_points = this->{control_points}->points();
	my $polygon = Qt::PolygonF(\@control_points);
	this->{polyline_item}->setPolygon($polygon);

	my $interpolator = this->{interpolator};
	my @bspline_points = $interpolator->interpolate(@control_points);
	my $bspline_polygon = Qt::PolygonF(\@bspline_points);
	this->{bspline_item}->setPolygon($bspline_polygon);
	this->{bspline_points} = \@bspline_points;

	changeModifiedFlag(1);
}

sub sceneMouseDoubleClicked
{
	my ($event) = @_;
	my $new_point = ControlPoint();
	$new_point->setPos($event->scenePos);
	this->{control_points}->addPoint($new_point);
	this->{graphics_scene}->addItem($new_point);
}

sub selectAll
{
	my @points = @{this->{control_points}->controlPoints()};

	foreach my $point (@points)
	{
		$point->setSelected(1);
	}
}

sub selectNone
{
	my @points = @{this->{control_points}->controlPoints()};

	foreach my $point (@points)
	{
		$point->setSelected(0);
	}
}

sub selectInvert
{
	my @points = @{this->{control_points}->controlPoints()};

	foreach my $point (@points)
	{
		$point->setSelected(!$point->isSelected());
	}
}

sub deleteSelection
{
	foreach my $point (this->{graphics_scene}->selectedPoints())
	{
		this->{graphics_scene}->removeItem($point);
		this->{control_points}->removePoint($point);
	}
}

sub sceneSelectionChanged
{
	my $data_model = this->{control_points};

	my $selection = Qt::ItemSelection();

	foreach my $point (this->{graphics_scene}->selectedPoints())
	{
		my $point_index = $data_model->pointIndex($point);
		$selection->select($point_index, $point_index);
	}

	this->{selection_model}->select($selection, Qt::ItemSelectionModel::Clear() | Qt::ItemSelectionModel::Select() | Qt::ItemSelectionModel::Rows());
}

sub tableSelectionChanged
{
	my ($selected, $deselected) = @_;

	my @control_points = @{this->{control_points}->controlPoints()};

	foreach my $index (@{$selected->indexes()})
	{
		@control_points[$index->row()]->setSelected(1);
	}

	foreach my $index (@{$deselected->indexes()})
	{
		@control_points[$index->row()]->setSelected(0);
	}
}

sub setFilename
{
	my ($filename) = @_;

	if ($filename ne this->{filename})
	{
		this->{filename} = $filename;

		emit filenameChanged($filename);
	}
}

sub clear
{
	setFilename('');
	controlPoints()->clear();
	graphicsScene()->clear();
	changeModifiedFlag(0);
}

sub load
{
	# If filename isn't specified, use document's filename
	my ($filename) = scalar(@_) >= 1 ? @_[0] : this->filename();

	# Try to open file for reading
	my $result = open(my $file, '<', $filename);
	if (!$result)
	{
		return 0;
	}

	# Read file contents
	my @lines = <$file>;

	# File should contain at least 4 lines
	if (scalar(@lines) ne 4)
	{
		close($file);
		return 0;
	}

	# Remove whitespace from lines
	for (my $i = 0; $i < scalar(@lines); $i++)
	{
		chomp(@lines[$i]);
	}

	# Check signature
	if (@lines[0] ne FILE_SIGNATURE)
	{
		close($file);
		return 0;
	}

	# Load parameters
	this->interpolator()->setOrder($lines[1]);
	this->interpolator()->setNumberOfPoints($lines[2]);

	# Delete control points from data model and scene
	this->controlPoints()->clear();
	this->graphicsScene()->clear();

	# Load control points
	my @serialized_points = split(';', $lines[3]);
	foreach my $serialized_point (@serialized_points)
	{
		my @coordinates = split(':', $serialized_point);
		my $point = ControlPoint();
		$point->setPos(@coordinates[0], @coordinates[1]);
		this->controlPoints()->addPoint($point);
		this->graphicsScene()->addItem($point);
	}

	# Close file
	close($file);

	# Update internal metadata
	setFilename($filename);

	# Clear modified flag
	changeModifiedFlag(0);

	# Indicate success
	return 1;
}

sub store
{
	# If filename isn't specified, use document's filename
	my $filename = scalar(@_) >= 1 ? @_[0] : this->filename();

	# Try to open file for writing
	my $result = open(my $file, '>', $filename);
	if (!$result)
	{
		return 0;
	}

	# Serialize control points
	my @points = controlPoints()->points();
	my @serialized_points = map { sprintf("%d:%d", $_->x(), $_->y()) } (@points);
	my $serialized_points = join(';', @serialized_points);

	# Write to file
	print($file FILE_SIGNATURE . "\n");
	print($file this->interpolator()->order() . "\n");
	print($file this->interpolator()->numberOfPoints() . "\n");
	print($file $serialized_points . "\n");

	# Close file
	close($file);

	# Update internal metadata
	setFilename($filename);

	# Clear modified flag
	changeModifiedFlag(0);

	# Indicate success
	return 1;
}

sub exportCurvePoints
{
	my ($filename) = @_;

	my $result = open(my $file, '>', $filename);
	if (!$result)
	{
		return 0;
	}

	my @points = @{this->{bspline_points}};
	my @serialized_points = map { sprintf("%d;%d", $_->x(), $_->y()) } (@points);

	print($file "X;Y\n" . join("\n", @serialized_points) . "\n");

	close ($file);

	return 1;
}

sub changeModifiedFlag
{
	my ($value) = @_;

	if ($value ne this->{modified})
	{
		this->{modified} = $value;

		emit modifiedChanged($value);
	}
}

1;
